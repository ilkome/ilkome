/*
	ilkome-gulp
	Version 3.1

	Ilya Komichev
	ilko.me
*/

/*
History
	v3.1
		- Added gulp plugin merge-stream
		- Added task for coopy css
		- Added task for copy favicon
*/

// Modules
// ===============================================
var gulp        = require("gulp"),
	watch       = require('gulp-watch'),
	requireDir  = require('require-dir'),
	runSequence = require('run-sequence'),
	merge       = require('merge-stream'),
	browsersync = require("browser-sync");


// Paths
// ===============================================
config = {
	tasks: './tasks',
	site: 'site',
	jade: {
		src: 'app/jade',
		inc: 'app/jade/inc',
		ignore: '!app/jade/inc/*.jade',
	},
	stylus: {
		src: 'app/stylus',
	},
	css: {
		src: 'app/stylesheets',
		build: 'site/css',
	},
	images: {
		src: 'app/images',
		build: 'site/img',
	},
	favicons: {
		src: 'app/favicons',
		build: 'site',
	}
}


// Require all tasks from gulpfile.js/tasks
// ===============================================
requireDir(config.tasks);


// Build task
// ===============================================
gulp.task('default', function(callback) {
	runSequence(
		'clean',
		['jade', 'stylus', 'images', 'css', 'favicon'],
		['watch', 'browsersync'],
		callback);
});


// Browsersync
// ===============================================
gulp.task('browsersync', function() {
	return browsersync.init({
		files: [config.site + '/*.html', config.css.build + '/*.css'],
		server: {baseDir: 'site'},
		open: false, //local
		notify: true,
		logFileChanges: false,
		notify: false
	});
});


// Watch
// ===============================================
gulp.task('watch', function() {
	// Process jade on change
	watch(config.jade.src + '/*.jade', function(event, cb) {
		gulp.start('jade');
	});

	// Process all jade when file form "inc" folder was change
	watch(config.jade.inc + '/*.jade', function(event, cb) {
		gulp.start('jade:nocache');
	});

	// Process stylus
	watch(config.stylus.src + '/**/*.styl', function(event, cb) {
		gulp.start('stylus');
	});

	// Copy CSS
	watch(config.css.src + '/**/*.css', function(event, cb) {
		gulp.start('css');
	});

	// Copy images
	watch(config.images.src + '/**/*.+(jpg|png|gif)', function(event, cb) {
		gulp.start('images');
	});
});