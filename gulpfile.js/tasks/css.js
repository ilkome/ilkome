// Modules
// ===============================================
var gulp = require("gulp");


// Task
// Just copy CSS to site css folder
// ===============================================
gulp.task('css', function() {
	return gulp.src([config.css.src + '/**/*.css'])

	// Save files
	.pipe(gulp.dest(config.css.build))
});