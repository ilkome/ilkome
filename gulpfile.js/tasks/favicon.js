// Modules
// ===============================================
var gulp = require("gulp");


// Task
// Just copy favicon to site folder
// ===============================================
gulp.task('favicon', function() {
	return gulp.src([config.favicons.src + '/**/*.png'])

	// Save files
	.pipe(gulp.dest(config.favicons.build))
});